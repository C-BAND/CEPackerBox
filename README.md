# CEPackerBox

CEPackerBox is sub useful tool provide by CopyEngine.   

it's aim to package dozen small bitmapdata together with an reasonable arithmetic.  

Packing things together is an old and interesting topic,you can find many many arithmetic by Google.  


## How It Work

What CEPackerBox use is pixel level collision detection.  

Yes, it is sound stupid.   

but it is quite powerful.  


## How To Use

first, you can use the SWC file directly, or you can improt the project ,and use it as your sub libray.

second,just put those code in , and CEPackerBox is ready to go!

```
	var builder:CEPackerBuilder=new CEPackerBuilder();
	builder.initialize(_allBitmapDataDic,_packerBoxNamePR);
	builder.startPackData();

	trace(builder.getAllPackerBoxVector())
```

+	*_allBitmapDataDic* 	is and Dictionary, contain all the bitmapData info that you wish CEPackerBox help you to pack.

		*Key* is the bitmapData Name you choice

		*Value* is the bitmapData source


+	*_packerBoxNamePR*		is the name you choice for the box prefix e.g. you choice "RedBox" for the PR, 

		the boxes generated name is	RedBox_1,RedBox_2,RedBox_3....



## Demo

```
// =================================================================================================
//
//	CopyEngine Framework
//	Copyright 2012 Eran. All Rights Reserved.
//
//	This program is free software. You can redistribute and/or modify it
//	in accordance with the terms of the accompanying license agreement.
//
// =================================================================================================

package
{
	import flash.display.BitmapData;
	import flash.display.Sprite;
	import flash.utils.Dictionary;

	import copyengine.utils.packerbox.CEPackerBuilder;
	import copyengine.utils.packerbox.box.ICEPackerBox;
	import copyengine.utils.packerbox.data.CEPackerElementData;

	public class CEPackerBoxDemo extends Sprite
	{
		public function CEPackerBoxDemo()
		{
			var allBitmapDataDic:Dictionary=new Dictionary();

			//Prepare test data
			for (var i:int=0; i < 10; i++)
			{
				allBitmapDataDic["name_" + i]=new BitmapData(100, 100);
			}

			//Start Packing
			var builder:CEPackerBuilder=new CEPackerBuilder();
			builder.initialize(allBitmapDataDic, "DemoBox");
			builder.startPackData();

			//Finish, HavaFun!

			for each (var box:ICEPackerBox in builder.getAllPackerBoxVector())
			{
				trace("------Box: " + box.getBoxName() + " Size : " + box.getBoxSize());
				for each (var data:CEPackerElementData in box.getAllBoxElement())
				{
					trace("              " + data.bitmapDataName + "       " + data.bitmapDataRe.toString());
				}
				trace("\n");
			}
		}
	}
}
```

Trace Info
```

------Box: DemoBox_1 Size : 256
              name_5       (x=0, y=0, w=100, h=100)
              name_1       (x=103, y=0, w=100, h=100)
              name_9       (x=0, y=102, w=100, h=100)
              name_7       (x=103, y=102, w=100, h=100)


------Box: DemoBox_2 Size : 256
              name_8       (x=0, y=0, w=100, h=100)
              name_4       (x=103, y=0, w=100, h=100)
              name_0       (x=0, y=102, w=100, h=100)
              name_3       (x=103, y=102, w=100, h=100)


------Box: DemoBox_3 Size : 256
              name_6       (x=0, y=0, w=100, h=100)
              name_2       (x=103, y=0, w=100, h=100)

```

## Issues

iamzealotwang@126.com

##Have fun!

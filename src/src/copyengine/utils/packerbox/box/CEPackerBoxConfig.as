// =================================================================================================
//
//	CopyEngine Framework
//	Copyright 2012 Eran. All Rights Reserved.
//
//	This program is free software. You can redistribute and/or modify it
//	in accordance with the terms of the accompanying license agreement.
//
// =================================================================================================

package copyengine.utils.packerbox.box
{

	public final class CEPackerBoxConfig
	{
		public static const DEFAULT_BOX_SIZE:int=256;
		public static const PADDING:int=2;

		public function CEPackerBoxConfig()
		{
		}
	}
}
